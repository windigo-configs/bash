Windigo's Bash Config
======

Welcome to my bash configurations! It's a little bit more verbose than
most, but I think it works pretty well.


File Location
------

I generally keep all of these files in `~/.config`, but you can put
them anywhere you like. Once you find your perfect place, symlink
the "rc" file to to the proper location that bash expects:

    ln -s <path to folder>/rc ~/.bashrc

If you're super fancy, you can modify your `.profile` or `.bash_profile`
files to load it automatically from the right place. Works for login shells,
like in [SDF](https://sdf.org):

    MY_BASHRC="$HOME/.config/bash/rc"
	if [ -n "$BASH_VERSION" ] && [ -f "$MY_BASHRC" ]; then
	    . "$MY_BASHRC"
	fi

Customization
------

At the moment the only local customizations available are *locale variables*.

The locale settings can be placed in a file called "`locale`" at the base
directory. They will be evaluated when the configuration is loaded and will
supercede the regular settings.

For instance, to set the timezone of your shell:

    ## Pacific Time
    TZ=America/Los_Angeles
   
    ## Eastern Time
    #TZ=America/New_York

You can use the `tzselect` command to get different timezone values.

Misc
------

If you have improvements, feel free to [let me know](https://fragdev.com). If
you like these configs, and have too much money, donate to the [Free Software
Foundation](https://fsf.org). They're cool.

~ Windigo, [fragdev.com](https://fragdev.com)
